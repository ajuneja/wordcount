import logging as log
import fileinput
import re
import os
from collections import defaultdict
import DirectoryWordCount.FileUtil as FileUtil
import logging
import sys
import string
class WordCounter:
	'''
		Scans all subfiles,compressed archives in a given root directory for files that match a given regex and generates word counts
	'''
	
	ERROR_EXPLORING_DIR = "error while exploring directory {} : {}"
	ERROR_INVALID_REGEX = "error invalid regex : {}"
	REGEX_ALPHA_NUMERIC_AND_WHITESPACES = r'([^\s\w]|_)+'

	def __init__(self,root_dir,regex=r'.*[.]txt'):
		self.configure_logger()
		self.logger.info("Creating WordCount with on directory : %s on all files matching : %s" ,root_dir,regex)
		if(FileUtil.is_valid_directory(root_dir)):	
			self._regex = regex
			self._root_dir = root_dir
			self.curr_dir = ""
			self.counts = defaultdict(int)
			self.temp_dirs = []

	
	def count_words(self):
		'''
				- Returns a dictionary of all words in a given directory + compressed archives
				- Removes all tmp directories created from opening archives on exit even if exception thrown
				- Exceptions include the current directory name where the exception was thrown for helpful error handling
		'''
		try:
			self.generate_word_count()
		except IOError as ioe:
			self.logger.error(str(ioe))
			raise IOError(WordCounter.ERROR_EXPLORING_DIR.format(self.curr,str(ioe)))
		except OSError as ose:
			self.logger.error(str(ose))
			raise OSError(WordCounter.ERROR_EXPLORING_DIR.format(self.curr,str(ose)))
		except re.error as r:
			self.logger.error(str(r))
			raise Exception(WordCounter.ERROR_INVALID_REGEX.format(str(r)))	
		except Exception as e:
			self.logger.error(str(e))
			raise Exception(WordCounter.ERROR_EXPLORING_DIR.format(self.curr,str(e)))
		finally:
			FileUtil.remove_dirs(self.temp_dirs)
			self.temp_dirs = []
		return self.counts

	def generate_word_count(self):
		'''
			Runs BFS over a filesystem tree starting at the provided root directory
			Processes files that match regex and builds dictionary of word counts
		'''
		stack = []
		visited = set()
		self.curr = ""
		self.counts = defaultdict(int)

		stack.append(self._root_dir)
		while(stack):
			self.curr = stack.pop()
			if(self.curr in visited):
				continue
			if(FileUtil.is_directory(self.curr)):
				stack = self.explore_directory(stack,self.curr)
			elif(FileUtil.is_compressed_file(self.curr)):
				new_temp_dir = FileUtil.uncompress(self.curr)
				stack.append(new_temp_dir)
				self.temp_dirs.append(new_temp_dir)
			elif(FileUtil.is_valid_file(self.curr,self._regex)):
				self.count_words_in_file(self.curr)
			visited.add(self.curr)

	def count_words_in_string(self,s):
		'''
			Counts all words in provided string s ignoring case and punctuation
		'''
		s = re.sub(WordCounter.REGEX_ALPHA_NUMERIC_AND_WHITESPACES, '', s)
		words = s.strip().lower().split(" ")
		for word in words:
			if(not word):
				continue
			self.counts[word] = self.counts[word] + 1
	
	def count_words_in_file(self,file):
		for line in fileinput.input(file):
			self.count_words_in_string(line)

	def explore_directory(self,stack,curr):
		'''
			Adds contents of current directory to search stack
		'''
		if (curr[-1] == "/"):	#Avoid duplicate / in filepath	
			curr = curr[:-1]
		files = [curr + "/" + f for f in os.listdir(curr)]
		self.logger.info("Adding %s files to search stack",files)
		stack = stack + files
		return stack

	def configure_logger(self):
		stdout_handler = logging.StreamHandler(sys.stdout)
		handlers = [stdout_handler]
		logging.basicConfig(
    		level=logging.DEBUG, 
    		format='[%(asctime)s] %(filename)s: %(levelname)s - %(message)s',
    		handlers=handlers)
		self.logger = logging.getLogger('WordCountLogger')