import unittest
import json
import pickle
from DirectoryWordCount.WordCounter import WordCounter
import sys
class TestWordCounter(unittest.TestCase):
	'''
		Tests the WordCounter libs ability to parse regex rules and generate correct word counts
		Requires Python3+
	'''
	@classmethod
	def setUpClass(cls):
		if sys.version_info < (3, 0):
			print("Requires Python3")
			sys.exit(1)


	def test_word_count_no_zip(self):
		'''
			Test reading words on the default regex .txt files in a directory without archives
		'''
		self.compare_test_with_expected("test-data/test-no-zip-files/test-data","test-data/test-no-zip-files/expected_counts.pickle")

	def test_regex_csv(self):
		'''
			Tests regex rule by scanning the same directory for test 1 for only csv files. Expected to be empty
		'''
		wc = WordCounter("test-data/test-no-zip-files/test-data",regex=r'.*[.].csv')
		expected_counts = wc.count_words()
		self.assertEqual(0,len(expected_counts))
	
	def test_word_count_two_levels_zip(self):
		'''
			Tests word count on directories archived into a zip file which is archived into another zip file
		'''
		self.compare_test_with_expected("test-data/test-2-layers-zip/test-data","test-data/test-2-layers-zip/expected_counts.pickle")
	
	def test_word_count_three_levels_zip(self):
		'''
			Tests word count on a mixture of files and directories and archives with the max archive level being 3 levels deep
		'''
		self.compare_test_with_expected("test-data/test-3-layers-zip/test-data","test-data/test-3-layers-zip/expected_counts.pickle")

	def compare_test_with_expected(self,test_directory,expected):
		'''
			Runs wordcount program and compares counts with an expected pickle file and asserts equality
		'''
		wc = WordCounter(test_directory)
		test_counts = wc.count_words()
		expected_file = open(expected, "rb")
		expected_counts = pickle.load(expected_file)
		expected_file.close()
		self.assertEqual(test_counts,expected_counts)


if __name__=='__main__':
	unittest.main()