import re
import os
import shutil
import zipfile
import uuid
'''
	Utility Class for File Handling
'''
ERR_NO_READ_ACCESS = "user does not have access to read for {}"
ERR_DOES_NOT_EXIST = "{} does not exist"

def is_compressed_file(file):
	compressed_suffix = [".zip"]  #add support for .tar .gz .7zip in future releases
	suffix = file.replace(' ','').split("/")[-1].lower()
	for s in compressed_suffix:
		if(re.match(".*"+s,suffix)):
			return True
	return False

def is_valid_file(file,pattern):
		filename = file.split("/")[-1]
		return os.path.isfile(file) and (re.match(pattern,filename) != None)

def is_valid_directory(root_dir):
		if(not os.path.isdir(root_dir)):
			raise IOError(ERR_DOES_NOT_EXIST.format(root_dir))
		if(not os.access(root_dir, os.R_OK)):
			raise IOError(ERR_NO_READ_ACCESS.format(root_dir))
		return True

def remove_dirs(dirs):
	for curr_dir in dirs:
		if(os.path.isdir(curr_dir)):
			shutil.rmtree(curr_dir) 

def uncompress(file):
	'''
		Uncompresses file into a random tmp directory
	'''
	zip_file = zipfile.ZipFile(file)
	temp_dir = os.path.dirname(file)+"/" + uuid.uuid4().hex
	zip_file.extractall(temp_dir)
	return temp_dir

def is_directory(path):
	return os.path.isdir(path)